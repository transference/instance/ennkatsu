__version__ = '0.0.1'
__channel__ = 'alpha'

import py_nextbus

import time
import json

import requests
import urllib

import traceback
import os
import glob


client = py_nextbus.NextBusClient(output_format='json')
class listener():
  def __init__(self, stop_tag=1462, route_tag='34', agency_tag='ttc'):
    self.config     = []
    self.config     = self.read_config()
    self.loc_time = 0
    self.file_num = self.config['start_file_num']
    self.sid_format = self.config['sid_format']
    self.data_path = self.config['data_path'].format(num=self.file_num)
    self.stop_tag   = stop_tag
    self.route_tag  = route_tag
    self.agency_tag = agency_tag
    self.vehicles   = {}
    files           = glob.glob(self.data_path.replace('{num}', '*'), recursive=False)
    for i in range(len(files)):
      file = files[i]
      self.vehicles.update(self.read_config(file))
    self.vehicles.update({'meta':{'version':__version__, 'channel':__channel__, 'start_time':int(time.time()), 'update_time':int(time.time())}})
  
  def read_config(self, path='./config.json'):
    with open(path) as file:
      try:
        return json.load(file)
      except json.decoder.JSONDecodeError as err:
        print('Daemon: JSON Decode failed.')
        print('Rela:   {}'.format(path))
        print('Abs:    {}'.format(os.path.abspath(path)))
        print(traceback.format_exc())
        if isinstance(err, urllib.error.URLError):
          print('Daemon: Sleeping 10 seconds (no connection to servers)')
          time.sleep(10)
  
  def write_config(self, obj, path='./config.json'):
    with open(path, 'w') as file:
      return json.dump(obj, file)
  
  def daemon(self):
    print('Daemon: Start')
    while not self.config['stop']:
      print('Daemon: Loop')
      print('Daemon: Read config')
      self.config = self.read_config()
      print('Config: {}'.format(self.config))
      if self.config['stop']:
        print('Daemon: Stop')
        return 'stop'
      print('Daemon: Call')
      try:
        self.loop(time_limit=self.config['time_limit'], time_interval=self.config['time_interval'])
      except Exception as err:
        print(traceback.format_exc())
        if isinstance(err, urllib.error.URLError):
          print('Daemon: Sleeping 10 seconds (no connection to servers)')
          time.sleep(10)
      print('Daemon: Update time')
      self.vehicles['meta']['update_time'] = int(time.time())
      print('Daemon: Write')
      self.write_config(self.vehicles, path=self.data_path.format(num=self.file_num))
    
  def loop(self, time_limit=20, time_interval=10):
    time_start = int(time.time())
    time_alt   = int(time.time())
    i = 1
    while time_alt-time_start < time_limit:
      print(i, time_alt-time_start)
      self._loop_all()
      time_now = int(time.time())
      j = 1
      while time_now-time_alt < time_interval:
        print('  {}'.format(j))
        time.sleep(1)
        time_now = int(time.time())
        j += 1
      i += 1
      time_alt = int(time.time())
  
  def _loop_all(self):
    self._loop_pred()
    self._loop_loc()
  
  def _loop_switch(self):
    if len(self.vehicles.keys()) > self.config['data_point_limit']:
      self.file_num += 1
  
  def _loop_pred(self):
    route = client.get_route_config(self.route_tag, self.agency)
    stops = route['stop']
    for stop in stops:
      preds = client.get_predictions(stop['stopId'], route_tag=self.route_tag, agency=self.agency_tag)['predictions']['direction']['prediction']
      for pred in preds:
        if all(i in pred for i in ('isDeparture', 'tripTag', 'vehicle', 'block', 'branch', 'dirTag', 'epochTime', 'seconds')):
          pred['lat'] = stop['lat']
          pred['lob'] = stop['lon']
          sid = self.sid_format.format(d=pred['dirTag'], i=pred['vehicle'])
          if sid == 'meta': sid = '\meta';
          if sid not in self.vehicles.keys():
            self.vehicles[sid] = {}
          if pred['epochTime'] not in self.vehicles[sid].keys():
            self.vehicles[sid][pred['epochTime']][stop['stopId']] = pred
          else:
            self.vehicles[sid][pred['epochTime']][stop['stopId']].update(pred)
  
  def _loop_loc(self):
    locs = client.get_vehicle_locations(self.route_tag, self.loc_time, agency=self.agency_tag)['vehicle']
    self.loc_time = int(time.time())
    for loc in locs:
      if 'dirTag' not in loc.keys() or 'secsSinceReport' not in loc.keys() or 'id' not in loc.keys() or 'lon' not in loc.keys() or 'lat' not in loc.keys():
        continue # Data is not desired
      sid = self.sid_format.format(d=loc['dirTag'], i=loc['id'])
      if sid == 'meta': sid = '\meta';
      loc['epochTime'] = self.loc_time - int(loc['secsSinceReport'])
      weather = self._get_weather(loc['lat'], loc['lon'])
      loc['temp'] = weather['main']['temp']
      loc['visibility'] = weather['visibility']
      loc['temp_min'] = weather['main']['temp_min']
      loc['temp_max'] = weather['main']['temp_max']
      loc['snow'] = {True: 1, False: 0}['13' in weather['weather'][0]['icon']]
      if '09' in weather['weather'][0]['icon']:
        loc['rain'] = 1
      elif '10' in weather['weather'][0]['icon']:
        loc['rain'] = 2
      else:
        loc['rain'] = 0
      if sid not in self.vehicles.keys():
        self.vehicles[sid] = {}
      if loc['epochTime'] not in self.vehicles[sid].keys():
        self.vehicles[sid][loc['epochTime']] = loc
      else:
        self.vehicles[sid][loc['epochTime']].update(loc)
  
  def _get_weather(self, lat, lon):
    r = requests.request(method='GET', url=self.config['openweathermap_current_weather_url'], params={'lat': lat, 'lon': lon, 'appid': self.config['openweathermap_api_key']})
    # r = requests.request('<accuweather_url>',
    #                      params={'q': '{},{}'.format(lat, lon), 'apikey': self.config['accuweather_api_key']})
    if str(r.status_code)[0] != '2':
      raise RuntimeError('OpenWeatherMap Returned non-2xx status code ({})'.format(r.status_code))
    data = json.loads(r.text)
    return data


if __name__ == '__main__':
  # print(client.get_vehicle_locations('34', 0, agency='ttc')['vehicle'])
  # print(client.get_predictions(1462, route_tag='34', agency='ttc')['predictions']['direction']['prediction'])
  inst = listener()
  inst.daemon()
  # inst.loop(time_limit=120)
  # import json
  # print(json.dumps(inst.vehicles, indent=2))
  