import json
import time


def to_csv(data):
  csvs = []
  csvs.append(data['meta'])
  csvs.append([data['meta']['version'], data['meta']['channel'], data['meta']['start_time'],
              data['meta']['update_time']])
  for i in range(len(data.keys())):
    max_i = len(data.keys())
    j = list(data.keys())[i]
    csv_data = [['dir', 'id', 'route', 'predable', 'time', 'hdg', 'spd', 'temp', 'vis',
            't_min', 't_max', 'snow', 'rain', 'lat', 'lon']]
    for k in range(len(data[j].keys())):
      max_k = len(data[j].keys())
      print('{} {}/{} {}/{}'.format(int(time.time()), i, max_i, k, max_k))
      try:
        l = list(data[list(data.keys())[i]].keys())[k]
        v = data[j][l]
        temp = [v['dirTag'], v['id'], v['routeTag'], v['predictable'], v['epochTime'],
                v['heading'], v['speedKmHr'], v['temp'], v['visibility'], v['temp_min'],
                v['temp_max'], v['snow'], v['rain'], v['lat'], v['lon']]
        csv_data.append(temp)
      except Exception:
        pass
      print(end='')
    csvs.append(csv_data)
  return csvs

def json_to_csv(string):
  print('Convert')
  data = json.loads(string)
  return to_csv(data)

if __name__ == '__main__':
  print('JSON to CSV')
  import csv
  import glob
  print('Ready.')
  open_path = 'data.json'#input('Open Path: ')
  save_path = 'data_n{i}_s{st}_u{ut}.csv'#input('Save Path Formula: ')
  print('Open')
  data = []
  files = glob.glob('data_*.json', recursive=False)
  for i in range(len(files)):
    path = files[i]
    with open(path, 'r') as file:
      data_ = json_to_csv(file.read())
      meta = data_[0]
      data_ = data_[2:]
      data += data_
  print('Save')
  for i in range(len(data)):
    max_i = len(data)
    print('{} {}/{}'.format(int(time.time()), i, max_i))
    with open(save_path.format(i=i, st=meta['start_time'], ut=meta['update_time']), 'w') as file:
      writer = csv.writer(file)
      writer.writerows(data[i])
  print('Done.')
  inp = input('Press [Enter] to exit')
  print('Exiting...')
  exit(0)
