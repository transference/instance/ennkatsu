#!/usr/bin/env python


import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="nb_wrapper", # Replace with your own username
    version="0.0.1",
    author="Ken S",
    author_email="colourdelete@gmail.com",
    description="A wrapper for the public NextBus API.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/transference/instance/nextbus",
    packages=setuptools.find_packages(),
    classifiers=[
        'Intended Audience :: Developers',
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.6',
)

# import os
# import sys

# import nextbus

# try:
#     from setuptools import setup
# except ImportError:
#     from distutils.core import setup

# if sys.argv[-1] == 'publish':
#     os.system('python setup.py sdist upload')
#     sys.exit()

# packages = [
#     'nextbus',
# ]

# requires = []

# readme =''
# history =''

# setup(
#     name='nb_wrapper',
#     version=nextbus.__version__,
#     description='python client for nextbus api',
#     long_description=readme + '\n\n' + history,
#     author='Ken S',
#     author_email='colourdelete@gmail.com',
#     url='http://python.org',
#     packages=packages,
#     package_data={'': ['LICENSE', 'NOTICE']},
#     package_dir={'nextbus': 'nextbus'},
#     include_package_data=True,
#     install_requires=requires,
#     license=license,
#     zip_safe=False,
#     classifiers=(
#         'Development Status :: 3 - Alpha',
#         'Intended Audience :: Developers',
#         'License :: OSI Approved :: Apache Software License',
#         'Programming Language :: Python',
#         'Programming Language :: Python :: 2.6',
#         'Programming Language :: Python :: 2.7',
#         'Programming Language :: Python :: 3',
#         'Programming Language :: Python :: 3.3',

#     ),
# )